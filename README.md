# sisop-praktikum-modul-2-2023-MH-IT20
Laporan pengerjaan soal shift modul 2 Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 |

## Soal 1

### Study case soal 1
---
John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut :

### Problem
---
a. Program tersebut menerima input path dengan menggunakan “argv”.

b. Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.

c. Program tersebut harus berjalan secara daemon.

d. Program tersebut akan terus berjalan di background dengan jeda 30 detik.

e. Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.

f. Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.”.

  - Example usage:
    > ./cleaner '/home/user/cleaner'
  - Example cleaner.log:
    > [2023-10-02 18:54:20] '/home/user/cleaner/is.txt' has been removed. <br>
    > [2023-10-02 18:54:20] '/home/user/cleaner/that.txt' has been removed. <br>
    > [2023-10-02 18:54:50] '/home/user/cleaner/who.txt' has been removed. <br>
  - Example valid sample file:
    > LJuUHmAnVLLCMRhLTcqy
 <br>bBp**SUSPICIOUS**agQKmLA <br>
    > BitSuQNHSLmZDvEcvbGc <br>
  - Example valid sample file:
    > LJuUHmAnVLLCMRhLTcqy
 <br>TFvefehhpWDCbkdirmlh <br>
    > BitSuQNHSLmZDvEcvbGc <br>

### Solution
---
a. Pertama membuat file cleaner.c yaitu program akan menerima input path dengan menngunakan "argv", dimana program tersebut bertugas untuk mengahpus file yang didalamnya mengandung string "SUSPECIOUS" pada direktori yang telah diinputkan oleh user.
Berikut adalah program cleaner.c yang saya gunakan:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>



int should_sleep = 1;

void write_log(const char *file_path) {
    time_t raw_time;
    struct tm *time_info;
    char time_str[20];
    time(&raw_time);
    time_info = localtime(&raw_time);
    strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", time_info);

    char log_entry[256];
    snprintf(log_entry, sizeof(log_entry), "[%s] '%s' has been removed.\n", time_str, file_path);

    openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "%s", log_entry);
    closelog();
}

void search_directory(const char *dir_path) {
    DIR *dir;
    struct dirent *entry;
    char path[1024];

    if ((dir = opendir(dir_path)) == NULL) {
        openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
        syslog(LOG_ERR, "Could not open directory: %s", dir_path);
        closelog();
        return;
    }-

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            snprintf(path, sizeof(path), "%s/%s", dir_path, entry->d_name);
            search_directory(path);
        } else {
            if (strstr(entry->d_name, ".txt") != NULL && strstr(entry->d_name, "SUSPICIOUS") != NULL) {
                snprintf(path, sizeof(path), "%s/%s", dir_path, entry->d_name);
                if (remove(path) == 0) {
                    write_log(path);
                    should_sleep = 0;
                } else {
                    openlog("cleaner", LOG_PID | LOG_NDELAY | LOG_NOWAIT | LOG_CONS, LOG_USER);
                    syslog(LOG_ERR, "Failed to remove file: %s", path);
                    closelog();
                }
                return;
            }
        }
    }

    closedir(dir);
}

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s '/path/to/your/directory'\n", argv[0]);
        return 1;
    }

    pid_t pid;

    pid = fork();
    if (pid < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    if (setsid() < 0) {
        perror("setsid failed");
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        perror("chdir failed");
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {

        should_sleep = 1;
        search_directory("/home/dhanixyi/inidir");

        if (should_sleep) {

            sleep(30);
        }
    }

    return 0;
}
```

b. Lalu saya mengompilasi program cleaner.c dengan perintah:
gcc cleaner.c -o cleaner 
c. Setelah proses mengompilasi selesai, saya bisa menjalankan program cleaner.c dengan perintah:
./cleaner /home/dhanixyi/inidir
disini program sedang berjalan menacri dan menghapus file .txt yang berisi SUSPECIOUS lalu menyimpan ke dalam log.
d. Program akan menemukan file .txt yang berisi SUSPECIOUS, lalu saya akan cek .log bahwa file .txt sudah benar benar berhasil di hapus dengan perintah:
nano /home/dhanixyi/cleaner.log
maka hasilnya seperti berikut:
[2023-10-12 23:40:14] '/home/dhanixyi/inidir/umpan.txt' has been removed.
[2023-10-12 23:40:14] '/home/dhanixyi/inidir/umpane.txt' has been removed.

### Kendala (Error)
---

### Revisi
---
1. Program belum bisa Daeomon

### Hasil
---
Program berhasil menghapus file yang didalamnya terdapat string "SUSPICIOUS" dan berjalan secara daemon dengan jeda 30 detik.
1. ada 4 file .txt dan 2 file berisi "SUSPECIOUS"
![File_txt_sebelum_dihapus](img/soal_1/File_txt_sebelum_dihapus.png)

2. proses_cari_dan_hapus_suspecious
![proses_cari_dan_hapus_suspecious](img/soal_1/proses_cari_dan_hapus_suspecious.png)

3. file log setelah berhasil menghapus suspecious
![log_berhasil_menghapus_suspecious](img/soal_1/log_berhasil_menghapus_suspecious.png)

## Soal 2

### Study case soal 2
---
QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:

### Problem
---
a. Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.

b. Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.

c. Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.

d. Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).

e. Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
- PG: {jumlah pemain}
- SG: {jumlah pemain}
- SF: {jumlah pemain}
- PF: {jumlah pemain}
- C: {jumlah pemain}

f. Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan.

g. Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.

Note:
- Format nama file yang akan diunduh dalam zip berupa [tim]-[posisi]-[nama].png
- Tidak boleh menggunakan system(), Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- 2 poin soal terakhir dilakukan setelah proses kategorisasi selesai.

### Solution
---

a. Pertama kali, kita disuruh untuk membuat file dengn nama Cavs.c yang merupakan program C untuk menjalankan perintah yang diperintahkan. Disini saya menggunakan IDE VsCode. Jadi tinggal menambah file didalam VsCode. Atau jika menggunakan Command Line dengan cara:
```c
nano cavs.c
```

b. kemudian kita download file .zip pada drive link dibawah ini:
```c
https://drive.google.com/file/d/1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK
```

Berikut adalah function-function yang digunakan untuk menjalankan program ini:

Function-function yang harus mengakses command dari system linux akan menggunakan fork untuk men-spawning proses, tiap-tiap command yang akan dieksekusi akan dijalankan di oleh proses child dan jika ternyata proses parent yang dijalankan duluan, makan akan mengeksekusi wait(Null). 

Dengan menggabungkan fork dan exec, kita dapat melakukan dua atau lebih tasks secara bersamaan.

Berikut merupakan program untuk mendowload file players.zip dan sekaligus membuat folder "players" tersebut:


```c
void downloadDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        
        char* const wgetCmd[] = {
            "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", 
            NULL
        };
        
        execvp("wget", wgetCmd);
        
        perror("Execvp failed");
        exit(1);
    } else {
        
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Download berhasil.\n");
        } else {
            printf("Download gagal.\n");
        }
    }
}

```
Setelah itu, di unzip file palyers.zip pada folder players yang sudah dibuat. Dengan menggunakan program fork() dan excevp dibawah ini:

```c
void extractDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        char* const unzipCmd[] = {
            "unzip", "players.zip", "-d", "players", 
            NULL
        };
        
        execvp("unzip", unzipCmd);
        
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Ekstraksi berhasil.\n");
        } else {
            printf("Ekstraksi gagal.\n");
        }
    }
}

void deleteZipDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        char* const rmCmd[] = {
            "rm", "players.zip", 
            NULL
        };
        
        execvp("rm", rmCmd);
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File ZIP berhasil dihapus.\n");
        } else {
            printf("File ZIP gagal dihapus.\n");
        }
    }
}
```
c. Berikut merupakan function fork() dan execvp untuk menghapus players yang bukan merupakan Clevelan Cavaliers:
```c
int isCavsPlayer(const char* filename) {
    return strstr(filename, "Cavaliers") != NULL;
}
```

dan berikut function void nya:
```c
void categorizePlayersCavs() {
    char* positions[] = {"PG", "SG", "SF", "PF", "C"};
    int playerCounts[5] = {0};

    FILE* output = fopen("Formasi.txt", "w");
    if (output == NULL) {
        perror("Failed to open Formasi.txt for categorize");
        exit(1);
    }

    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    // Fungsi untuk menghapus pemain yang bukan dari Cleveland Cavaliers
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            if (!isCavsPlayer(entry->d_name)) {
                char filePath[512]; 
                snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);
                // Menghapus file yang tidak termasuk dalam pemain Cleveland Cavaliers
                remove(filePath);  
            }
        }
    }
    
    closedir(dir);
```

d. Kemudian berikut merupakan function untuk menghitung pemain Cleveland Cavaliers di dalam file formasi.txt. secara otomatis pada funtion berikut:

```c
 // Inisialisasi hitungan pemain untuk setiap posisi
    int Count_PG = 0, Count_SG = 0, Count_SF = 0, Count_PF = 0, Count_C = 0;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    // Fungsi untuk menghitung pemain Cavaliers berdasarkan posisi "PG", "SG", "SF", "PF", "C"
    while ((entry = readdir(dir)) != NULL) {
        if (strstr(entry->d_name, "Cavaliers") != NULL && strstr(entry->d_name, ".png") != NULL) {
            char filepath[512];
            char PosisiPlayers[10];
            snprintf(filepath, sizeof(filepath), "players/%s", entry->d_name);
            sscanf(entry->d_name, "Cavaliers-%2s", PosisiPlayers);
            char Categorize_folder[32];
            snprintf(Categorize_folder, sizeof(Categorize_folder), "players/%s", PosisiPlayers);

            struct stat st = {0};
            if (stat(Categorize_folder, &st) == -1) {
                mkdir(Categorize_folder, 0777);
            }

            char New_Folders[512];
            snprintf(New_Folders, sizeof(New_Folders), "%s/%s", Categorize_folder, entry->d_name);
            rename(filepath, New_Folders);

            if (strcmp(PosisiPlayers, "PG") == 0) Count_PG++;
            else if (strcmp(PosisiPlayers, "SG") == 0) Count_SG++;
            else if (strcmp(PosisiPlayers, "SF") == 0) Count_SF++;
            else if (strcmp(PosisiPlayers, "PF") == 0) Count_PF++;
            else if (strcmp(PosisiPlayers, "C-") == 0) Count_C++;
        }
    }
    closedir(dir);
    // Menulis informasi hitungan players ke dalam Formasi.txt
    fprintf(output, "PG: %d\n", Count_PG);
    fprintf(output, "SG: %d\n", Count_SG);
    fprintf(output, "SF: %d\n", Count_SF);
    fprintf(output, "PF: %d\n", Count_PF);
    fprintf(output, "C: %d\n", Count_C);

    fclose(output);
}
```

Dengan hasil output dalam file formasi.txt yaitu:
```c
PG: 4
SG: 3
SF: 4
PF: 1
C: 3
```
d. Setelah itu, pada soal disuruh mengkategorikan players sesuai dengan posisi masing" yaitu "PG", "SG", "SF", "PF", "C". Pertama membuat folder kategori sesuai posisi tersebut dengan cara:
```c
//Fungsi untuk mengkategorikan players sesuai dengan posisi masing"
void createCategoryFolderPlayersCavs() {
    
    mkdir("players/PG", 0755);
    mkdir("players/SG", 0755);
    mkdir("players/SF", 0755);
    mkdir("players/PF", 0755);
    mkdir("players/C-", 0755);
}
```
Kemudian masukkan foto pemain Cleveland Cavaliers dengan menggunakan program code sebagai berikut:
```c
void categorizePngFilePlayers() {
    char* positions[] = {"PG", "SG", "SF", "PF", "C"};
    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);

            if (isCavsPlayer(entry->d_name)) {
                for (int i = 0; i < 5; i++) {
                    if (strstr(entry->d_name, positions[i]) != NULL && strstr(entry->d_name, ".") != NULL) {
                        char destPath[512];
                        snprintf(destPath, sizeof(destPath), "players/%s/%s", positions[i], entry->d_name);
                        rename(filePath, destPath);
                    }
                }
            }
        }
    }

    closedir(dir);
}
```

f. Pada 2 soal terakhir diperintahkan untuk memajang sebuah foto yang termasuk " pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7" dan "pemain yang melakukan The Block pada ajang yang sama" di dalam folder "clutch".

Pertama membuat terlebih dahulu folder "clutch" dengan cara:
```c
void createClutchFolder() {
    mkdir("clutch", 0755);
}
```

Setelah itu kita pindahkan pemain yang bernama "Kyrie-Irving" yang merupakan 
pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7" dan "LeBron-James" yang merupakan "pemain yang melakukan The Block pada ajang yang sama" Dengan program berikut:
```c
void movePlayersCavsToClutchFolder() {
    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);

            if (isCavsPlayer(entry->d_name)) {
                if (strstr(entry->d_name, "LeBron-James") != NULL || strstr(entry->d_name, "Kyrie-Irving") != NULL) {
                    char destPath[512];
                    snprintf(destPath, sizeof(destPath), "clutch/%s", entry->d_name);
                    rename(filePath, destPath);
                }
            }
        }
    }

    closedir(dir);
}
```

Setelah semua selesai jadikan satu function void ke dalam "int main()" untuk melakukan perintah program yang diperintahkan, untuk menghasilkan output yang diinginkan dengan cara sebagai berikut:
```c
int main() {
    pid_t child_pid;

    child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        downloadDriveNBA();
        extractDriveNBA();
        deleteZipDriveNBA();
        categorizePlayersCavs();
    createCategoryFolderPlayersCavs();
        createClutchFolder();
        movePlayersCavsToClutchFolder();
        sleep(2);
categorizePngFilePlayers();
        printf("Selesai\n");
    } else {
        wait(NULL);
    }

    return 0;
}
```
Jadikan satu program-program diatas pada satu file yang bernama "cavs.c" untuk menjalankan semua perintahnya kemudian dijalankan pada VsCode dengan cara:
```c
Ctrl+Alt+n
```

### Kendala (Error)
---
1. Tidak bisa memasukkan foto "LeBron-James.png" dan foto "Kyrie-Irving.png" ke dalam folder "clutch".


### Revisi
---
---
> Program yang ditambahkan setelah demo soal shift 2 dilakukan

1. Sudah bisa memindahkan foto "Kyrie-Irving.png" dan "Lebron-James.pnh" pada folder "clutch".

### Hasil
---
1. Result Program Categorize_Players_Cavaliers

![Hasil Categorize_Plaers_Cavs](img/soal_2/Hasil_Categorize_Folder.png)

2. Result Formasi.txt
![Hasil Formasi.txt](img/soal_2/Hasil_Formasi.txt.png)

3. Result Clutch_Folder 
![Hasil Clutch_Folder](img/soal_2/Hasil_Clutch_Folder.png)

## Soal 3

### Study case soal 3
---
Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

### Problem
---
a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).

d. Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Note :
- Tidak boleh menggunakan system().
- Proses berjalan secara daemon.
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping).

### Solution
---
a.membuat folder dengan nama timestamp dalam format [YYYY-MM-dd_HH:mm:ss]. Ini mencakup pengambilan waktu saat ini, konversi waktu ke dalam struktur tm, pembentukan nama folder, dan pembuatan folder dengan hak akses yang sesuai.

```c
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <zip.h>
#include <sys/stat.h>

void bikinfolder(char *nmfolder) {
    struct tm *statuswaktu;
    char timestamp[20];
    time_t waktuskrg;

    time(&waktuskrg);
    statuswaktu = waktulokal(&waktuskrg);

    strftime(timestamp, sizeof(timestamp), "[%Y-%m-%d_%H:%M:%S]", statuswaktu);

    mkdir(timestamp, 0777);

}
```

b. Mengonversi nama folder timestamp ke dalam nilai epoch Unix. Ini melibatkan ekstraksi tahun, bulan, hari, jam, menit, dan detik dari nama folder, dilanjutkan dengan penyesuaian tahun dan bulan yang dimulai dari nilai tertentu.

```c
time_t ubahepoch(const char *nmfolder) {
    struct tm waktu_stat;
    memset(&waktu_stat, 0, sizeof(struct tm));

    sscanf(nmfolder, "[%d-%d-%d_%d:%d:%d]",
           &waktu_stat.tm_thn, &waktu_stat.tm_bln, &waktu_stat.tm_hari,
           &waktu_stat.tm_jam, &waktu_stat.tm_mnt, &waktu_stat.tm_dtk);

    waktu_stat.tm_thn -= 1900;
    waktu_stat.tm_bln--;

    return mktime(&waktu_stat);
}
```

c. membuat file ZIP dan menambahkan file-file dari folder ke dalamnya. Ini melibatkan pembukaan file ZIP, pembukaan direktori untuk membaca file-filenya, dan penambahan file ke dalam ZIP. Juga, penanganan kesalahan saat pembukaan ZIP atau direktori gagal.

```c
int masukzip(const char *namazip, const char *nmfolder) {
    int err;
    struct zip *za;
    za = zip_open(namazip, ZIP_CREATE | ZIP_TRUNCATE, &err);
    if (!za) {
        fprintf(stderr, "tidak dapat membuka zip: %s\n", zip_strerror(za));
        return -1;
    }

    DIR *dir;
    struct dirent *entry;

    dir = opendir(nmfolder);
    if (dir == NULL) {
        perror("tidak dapat membuka folder");
        zip_close(za);
         return -1;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { 
            char filePath[200];
            snprintf(filePath, sizeof(filePath), "%s/%s", nmfolder, entry->d_name);
            if (zip_file_add(za, entry->d_name, filePath, ZIP_FL_ENC_UTF_8) < 0) {
                fprintf(stderr, "tidak dapat add file ke dalam ZIP: %s\n", zip_strerror(za));
            }
        }
    }

    int hapusfolder(const char *nmfolder) {
    if (rmdir(nmfolder) == 0) {
        return 0; 
    } else {
        perror("Error");
        return -1;
    }
}

    closedir(dir);

    if (zip_close(za) < 0) {
        fprintf(stderr, "tidak dapat menutup ZIP: %s\n", zip_strerror(za));
        return -1;
    }

    return 0;
}
```

d. fungsi main terdapat loop utama yang mencakup pembuatan folder timestamp, pengunduhan gambar, dan pembuatan file ZIP. Program berjalan dalam loop tak berhenti (while (1)) untuk terus-menerus menjalankan tugasnya.

```c
int main() {
    struct tm *statuswaktu;
    char nmfolder[20];

    while (1) {
        bikinfolder(nmfolder);

        time_t epoch_unix = ubahepoch(nmfolder);
        statuswaktu = waktulokal(&epoch_unix);

        for (int i = 0; i < 15; i++) {
            time_t current_time = time(NULL);
            struct tm *statuswaktu = waktulokal(&current_time);

            int width = (epoch_unix % 1000) + 50;
            int height = (epoch_unix % 1000) + 50;

            char namapcture[50];  
            snprintf(namapcture, sizeof(namapcture), "[%04d-%02d-%02d_%02d:%02d:%02d].jpg",
            statuswaktu->tm_thn + 1900, statuswaktu->tm_bln + 1, statuswaktu->tm_hari,
            statuswaktu->tm_jam, statuswaktu->tm_mnt, statuswaktu->tm_dtk);

            char url[100];
            snprintf(url, sizeof(url), "https://source.unsplash.com/%dx%d", width, height);

            pid_t child_pid = fork();
            if (child_pid == 0) {

                char pathpcture[200];
                snprintf(pathpcture, sizeof(pathpcture), "./%s/%s", nmfolder, namapcture);

                execlp("wget", "wget", "-O", pathpcture, url, NULL);

                perror("execlp");
                exit(EXIT_FAILURE);
            } else if (child_pid > 0) {
                wait(NULL);
            } else {
                perror("fork");
                exit(EXIT_FAILURE);
            }
            sleep(5);
        }
        sleep(30);

        char namazip[50];
        snprintf(namazip, sizeof(namazip), "%s.zip", nmfolder);

        if (masukzip(namazip, nmfolder))
    }
    return 0;
}
```

### Kendala (Error)
---

### Revisi
---
a. Menerima dua argumen baris perintah (argc adalah jumlah argumen, argv adalah array argumen). Program membuka file "kill.sh" dan menuliskan perintah shell ke dalamnya berdasarkan argumen yang diberikan. Jika argumen adalah -a, maka perintah "killall -9 lukisan" ditambahkan ke file. Jika argumen adalah -b, perintah untuk mencari dan menghentikan proses dengan nama "lukisan" ditambahkan. Setelah itu, program mengeksekusi perintah chmod +x kill.sh untuk membuat file "kill.sh" dapat dieksekusi.
Program kemudian menciptakan proses anak chi untuk mengeksekusi perintah chmod +x kill.sh. Jika proses ini berhasil dibuat, maka program akan keluar.
Selanjutnya, program menciptakan proses orangtua par. Jika proses ini berhasil dibuat, maka program akan keluar.
Program mengatur ulang hak akses file dengan umask(0).
Program menciptakan proses daemon dengan menggunakan setsid(), melepaskan diri dari terminal utama, dan menutup file descriptor standar (STDIN, STDOUT, STDERR).

b. Selanjutnya akan memasuki loop utama yang akan berjalan selamanya dan menciptakan proses anak make_folder yang akan digunakan untuk membuat folder dengan timestamp dan melibatkan pengambilan waktu saat ini dan pemformatan waktu dalam format tertentu yang akan digunakan sebagai nama folder. Selanjutnya pada proses anak make_folder mencoba membuat folder dengan nama timestamp menggunakan perintah shell mkdir.


### Hasil
---
1. Result 1

![Hasil 1](img/soal_3/Result_1.jpg)

2. Result 2
![Hasil 2](img/soal_3/Result_2.jpg)

3. Result 3 
![Hasil 3](img/soal_3/Results_3.jpg)

## Soal 4

### Study case soal 4
---
Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

### Problem
---
a. Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system()
Daftar ekstensi file yang dianggap virus tersimpan dalam file extensions.csv.

b. Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:

[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}

Contoh:  [sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine

*nama_user: adalah username dari user yang menambahkan file ter-infected

c. Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.

d. Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.
- Low: Hanya me-log file yg terdeteksi
- Medium: log dan memindahkan file yang terdeteksi
- Hard: log dan menghapus file yang terdeteksi

ex: **./antivirus -p low**

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:
  - Example usage:
    > kill -SIGUSR1 <pid_program>

e. Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

### Solution
---
a. Pertama kali, kita disuruh untuk Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. yang merupakan program C untuk menjalankan perintah memeriksa file di folder sisop_infected. Disini saya menggunakan IDE VsCode. Jadi tinggal menambah file didalam VsCode. Atau jika menggunakan Command Line dengan cara:
```c
nano antivirus.c
```

b. kemudian kita download file .csv pada drive link dibawah ini:
```c
https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5
```

Berikut adalah function-function yang digunakan untuk menjalankan program ini:

Function-function yang harus mengakses command dari system linux akan menggunakan fork untuk men-spawning proses, tiap-tiap command yang akan dieksekusi akan dijalankan di oleh proses child dan jika ternyata proses parent yang dijalankan duluan, makan akan mengeksekusi wait(Null). 

Dengan menggabungkan fork dan exec, kita dapat melakukan dua atau lebih tasks secara bersamaan.

Berikut merupakan program untuk mendowload file extensions.csv tersebut:
```c
void downloadExtension() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        execlp("wget", "wget", "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5", "-O", "extension.csv", (char *) NULL);
        
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Download berhasil.\n");
        } else {
            printf("Download gagal.\n");
        }
    }
}
```
b. Kemudian kan pada soal hanya 8 baris pertama yang tidak dienkripsi, dan baris-baris setelahnya perlu anda deskripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya, dengan cara code berikut:
```c
// Fungsi untuk melakukan dekripsi ROT13
void rot13(char *str) {
    for (int i = 0; str[i]; i++) {
        char c = str[i];
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            if ((c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M')) {
                str[i] += 13;
            } else {
                str[i] -= 13;
            }
        }
    }
}
```
Kemudian untuk mencata informasi pada file "virus.log" sekaligus memindahkannya ke dalam file "quarantine" menggunakan code berikut:
```c
int main() {
    createDirectory("sisop_infected"); 
    downloadExtension();

    char **infectedExtensions = NULL;
    int numExtensions = 0;

    // Fungsi untuk membaca daftar ekstensi dari file extension.csv dan dekripsi jika diperlukan
    FILE *extensionFile = fopen("extensions.csv", "r"); // Fix the filename
    if (!extensionFile) {
        perror("Error membuka file extensions.csv"); // Fix the error message
        return 1;
    }

    char buffer[256];
    while (fgets(buffer, sizeof(buffer), extensionFile)) {
        // Fungsi untuk menghapus karakter newline dari ekstensi
        strtok(buffer, "\n");

        // Dekripsi ekstensi jika diperlukan
        rot13(buffer);

        // Fungsi untuk memperluas array infectedExtensions
        char **temp = realloc((char **)infectedExtensions, (numExtensions + 1) * sizeof(char *));
        if (!temp) {
            perror("Error alokasi memori");
            fclose(extensionFile);
            free((char **)infectedExtensions);
            return 1;
        }
        infectedExtensions = temp;

        // Fungsi untuk menambahkan ekstensi ke daftar terinfeksi
        infectedExtensions[numExtensions] = strdup(buffer);
        numExtensions++;
    }

    fclose(extensionFile);
```
c. Kemudian Choco memrlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini secara otomatis memeriksa folder "sisop_infected" setiap detik:
```c
// Fungsi waktu untuk pencatatan waktu log
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char logTimestamp[20];
    strftime(logTimestamp, sizeof(logTimestamp), "%d-%m-%y:%H-%M-%S", tm_info);

    FILE *virusLog = fopen("virus.log", "a");
    if (!virusLog) {
        perror("Error membuka virus.log");
        free((char **)infectedExtensions);
        return 1;
    }

    const char *infectedDir = "sisop_infected";
    const char *quarantineDir = "quarantine";

    DIR *dir = opendir(infectedDir);
    if (!dir) {
        perror("Error membuka direktori sisop_infected");
        free((char **)infectedExtensions);
        fclose(virusLog);
        return 1;
    }

    struct dirent *entry;
    while ((entry = readdir(dir))) {
        char sourcePath[256];
        snprintf(sourcePath, sizeof(sourcePath), "%s/%s", infectedDir, entry->d_name);

        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue; 
        }

        if (isInfected(entry->d_name, (const char **)infectedExtensions, numExtensions)) {
            pid_t child_pid = fork();
            if (child_pid == -1) {
                perror("Error saat melakukan fork");
                closedir(dir);
                free((char **)infectedExtensions);
                fclose(virusLog);
                return 1;
            }

            if (child_pid == 0) { 
                char *argv[] = {"./antivirus", (char *)sourcePath, (char *)quarantineDir, NULL};
                perror("Error saat menjalankan program antivirus");
                exit(1);
            } else { 
                int status;
                waitpid(child_pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                    fprintf(virusLog, "[sisopUser][%s] - %s - Moved to quarantine\n", logTimestamp, entry->d_name);
                }
            }
        }
    }

    closedir(dir);
    fclose(virusLog);
    free((char **)infectedExtensions);

    return 0;
}
```
e. Kemudian yang terakhir untuk menjalankan antivirus, perlu menonaktifkannya sementara dengan cara:
```c
pkill antivirus
```

### Kendala (Error)
---
- Bisa menjalankan antivirus.c tetapi, program tidak berjalan dan berhenti tidak bisa untuk di .daemon. serta tidak tercatat juga di dalam file "virus.log"

### Revisi
---
> Program yang ditambahkan setelah demo soal shift 2 dilakukan

1. Sudah bisa menjalankan program "antivirus.c" dan program berjalan dengan lancar, dan dicatat di dalam "virus.log"

### Hasil
---
1. Result Program ./programCodeRunner-p low

![Hasil ./programCodeRunner-p low](img/soal_4/Hasil_Extension.png)

2. Result Virus.Log
![Hasil Virus.Log.txt](img/soal_4/Hasil_Virus.loh.png)




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

// Fungsi untuk mendownload file extension.csv ke dalam folder soal2
void downloadExtension() {
    pid_t child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        execlp("wget", "wget", "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5", "-O", "extensions.csv", (char *) NULL);

        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Download berhasil.\n");
        } else {
            printf("Download gagal.\n");
        }
    }
}

// Fungsi untuk memeriksa apakah ekstensi file ada dalam daftar ekstensi yang telah diberikan
int isInfected(const char *extension, const char *infectedExtensions[], int numExtensions) {
    for (int i = 0; i < numExtensions; i++) {
        if (strcmp(extension, infectedExtensions[i]) == 0) {
            return 1; 
        }
    }
    return 0; 
}

// Fungsi untuk membuat folder sisop_infected
void createDirectory(const char *sisop_infected) {
    struct stat st;
    if (stat(sisop_infected, &st) == -1) {
        if (mkdir(sisop_infected, 0755) != 0) {
            perror("Error saat membuat folder");
            exit(1);
        }
    }
}

// Fungsi untuk melakukan dekripsi ROT13
void rot13(char *str) {
    for (int i = 0; str[i]; i++) {
        char c = str[i];
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            if ((c >= 'a' && c <= 'm') || (c >= 'A' && c <= 'M')) {
                str[i] += 13;
            } else {
                str[i] -= 13;
            }
        }
    }
}

int main() {
    createDirectory("sisop_infected"); 
    downloadExtension();

    char **infectedExtensions = NULL;
    int numExtensions = 0;

    // Fungsi untuk membaca daftar ekstensi dari file extension.csv dan dekripsi jika diperlukan
    FILE *extensionFile = fopen("extensions.csv", "r"); // Fix the filename
    if (!extensionFile) {
        perror("Error membuka file extensions.csv"); // Fix the error message
        return 1;
    }

    char buffer[256];
    while (fgets(buffer, sizeof(buffer), extensionFile)) {
        // Fungsi untuk menghapus karakter newline dari ekstensi
        strtok(buffer, "\n");

        // Dekripsi ekstensi jika diperlukan
        rot13(buffer);

        // Fungsi untuk memperluas array infectedExtensions
        char **temp = realloc((char **)infectedExtensions, (numExtensions + 1) * sizeof(char *));
        if (!temp) {
            perror("Error alokasi memori");
            fclose(extensionFile);
            free((char **)infectedExtensions);
            return 1;
        }
        infectedExtensions = temp;

        // Fungsi untuk menambahkan ekstensi ke daftar terinfeksi
        infectedExtensions[numExtensions] = strdup(buffer);
        numExtensions++;
    }

    fclose(extensionFile);

    // Fungsi waktu untuk pencatatan waktu log
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char logTimestamp[20];
    strftime(logTimestamp, sizeof(logTimestamp), "%d-%m-%y:%H-%M-%S", tm_info);

    FILE *virusLog = fopen("virus.log", "a");
    if (!virusLog) {
        perror("Error membuka virus.log");
        free((char **)infectedExtensions);
        return 1;
    }

    const char *infectedDir = "sisop_infected";
    const char *quarantineDir = "quarantine";

    DIR *dir = opendir(infectedDir);
    if (!dir) {
        perror("Error membuka direktori sisop_infected");
        free((char **)infectedExtensions);
        fclose(virusLog);
        return 1;
    }

    struct dirent *entry;
    while ((entry = readdir(dir))) {
        char sourcePath[256];
        snprintf(sourcePath, sizeof(sourcePath), "%s/%s", infectedDir, entry->d_name);

        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue; 
        }

        if (isInfected(entry->d_name, (const char **)infectedExtensions, numExtensions)) {
            pid_t child_pid = fork();
            if (child_pid == -1) {
                perror("Error saat melakukan fork");
                closedir(dir);
                free((char **)infectedExtensions);
                fclose(virusLog);
                return 1;
            }

            if (child_pid == 0) { 
                char *argv[] = {"./antivirus", (char *)sourcePath, (char *)quarantineDir, NULL};
                perror("Error saat menjalankan program antivirus");
                exit(1);
            } else { 
                int status;
                waitpid(child_pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                    fprintf(virusLog, "[sisopUser][%s] - %s - Moved to quarantine\n", logTimestamp, entry->d_name);
                }
            }
        }
    }

    closedir(dir);
    fclose(virusLog);
    free((char **)infectedExtensions);

    return 0;
}
